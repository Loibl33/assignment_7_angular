import {Injectable, OnInit} from '@angular/core';
import {pokemon} from '../models/pokemon.model';
import {StorageUtil} from "../utils/storage.utils";
import {Trainer} from "../models/trainer.model";
import {StorageKeys} from "../enums/storage-keys.enum";
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class PokemonService implements OnInit {
  private _trainer?: Trainer;
  pokemonsArrayRaw: pokemon[] | undefined
  constructor() {
    this._trainer = StorageUtil.storageRead<Trainer>(StorageKeys.Trainer);
  }
  async ngOnInit() {
    this.pokemonsArrayRaw = []
    this.pokemonsArrayRaw = await fetchPokemons(this, 1000, 0)
  }

  async addPokemon(userId : number | undefined, pokename: String | undefined){

    let trainer = await fetch(`${environment.apiURL}/trainers/${userId}`)
    let trainerJs = await trainer.json()
    let pokes = await trainerJs.pokemon

    fetch(`${environment.apiURL}/trainers/${userId}`, {
      method: 'PATCH', // NB: Set method to PATCH
      headers: {
        'X-API-Key': environment.apiKey,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        // Provide new Pokémon to add trainer with id 1
        pokemon: [...pokes, pokename]
      })
    })
      .then(response => {
        if (!response.ok) {
          throw new Error('Could not update trainer')
        }
        return response.json()
      })
      .then(updatedTrainer => {
        StorageUtil.storageSave<Trainer>(StorageKeys.Trainer, updatedTrainer);
      })
      .catch(error => {
      })
  }

  async removePokemon(userId:number | undefined, pokename: String | undefined){

    let trainer = await fetch(`${environment.apiURL}/trainers/${userId}`)
    let trainerJs = await trainer.json()
    let pokes = await trainerJs.pokemon

    fetch(`${environment.apiURL}/trainers/${userId}`, {
      method: 'PATCH', // NB: Set method to PATCH
      headers: {
        'X-API-Key': environment.apiKey,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        // Provide new Pokémon to add trainer with id 1
        pokemon: pokes.filter((r: String | undefined) => r != pokename)
      })
    })
      .then(response => {
        if (!response.ok) {
          throw new Error('Could not update trainer')
        }
        return response.json()
      })
      .then(updatedTrainer => {
        StorageUtil.storageSave<Trainer>(StorageKeys.Trainer, updatedTrainer);
      })
      .catch(error => {
      })
  }

}
export async function fetchPokemons(s: any, limit: number, offset: number) {
  let res = await fetch(`https://pokeapi.co/api/v2/pokemon?limit=` + limit + `&offset=` + offset)
  let resp = await res.json()
  return resp
    .results
    .map((poke: any) =>
      ({
        name: poke.name,
        imageURL: ("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"
          + poke.url.split("/")[6]
          + ".png")
      }));
}

export async function fetchPokemonByName(name: String): Promise<pokemon>{

  let res = await fetch(`https://pokeapi.co/api/v2/pokemon/` + name)
  let resp = await res.json()
  let ret = {name:name, imageURL: ("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + resp.sprites.back_default.split("/")[resp.sprites.back_default.split("/").length -1]) }
  return ret
}


