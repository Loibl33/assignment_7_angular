import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatTableModule} from '@angular/material/table';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { PokemonGridComponent } from './components/pokemon-grid/pokemon-grid.component';
import {MatGridListModule} from '@angular/material/grid-list';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule} from '@angular/common/http';

import { LandingPage } from './pages/landing-page/landing-page';
import { CataloguePage } from './pages/catalogue-page/catalogue-page';
import { TrainerPage } from './pages/trainer-page/trainer-page';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { CatalogueComponent } from './components/catalogue/catalogue.component';
import {MatMenuModule} from '@angular/material/menu';

// Decorator
@NgModule({
  declarations: [ // Components
    AppComponent,
    ToolbarComponent,
    PokemonGridComponent,
    LandingPage,
    CataloguePage,
    TrainerPage,
    LoginFormComponent,
    CatalogueComponent
  ],
  imports: [ // Modules
    HttpClientModule,
    BrowserModule,
    MatToolbarModule,
    MatIconModule,
    MatTableModule,
    MatGridListModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatInputModule,
    MatMenuModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
