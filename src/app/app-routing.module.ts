import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./guards/auth.guard";
import { CataloguePage } from "./pages/catalogue-page/catalogue-page";
import { LandingPage } from "./pages/landing-page/landing-page";
import { TrainerPage } from "./pages/trainer-page/trainer-page";

const routes: Routes = [
    {
        path: "",
        pathMatch: "full",
        redirectTo: "/landing"
    },
    {
        path: "landing",
        component: LandingPage
    },
    {
        path: "trainer",
        component: TrainerPage,
        canActivate: [ AuthGuard]
    },
    {
        path: "catalogue",
        component: CataloguePage,
        canActivate: [ AuthGuard ]
    }
]

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ], // Import module
    exports: [
        RouterModule
    ] // Expose module and features
})
export class AppRoutingModule {}