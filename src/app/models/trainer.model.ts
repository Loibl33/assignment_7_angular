import { pokemon } from "./pokemon.model";
export type Trainer = {
  id: number;
  username: String;
  pokemon: pokemon[]
}
