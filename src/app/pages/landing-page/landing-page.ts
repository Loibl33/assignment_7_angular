import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StorageKeys } from 'src/app/enums/storage-keys.enum';
import { Trainer } from 'src/app/models/trainer.model';
import { StorageUtil } from '../../utils/storage.utils'

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.html',
  styleUrls: ['./landing-page.css']
})

export class LandingPage implements OnInit {
  private _trainer?: Trainer;

  constructor(private readonly router: Router) 
    {this._trainer = StorageUtil.storageRead<Trainer>(StorageKeys.Trainer);}

  ngOnInit(): void {
  }

  handleLogin() : void {
    this.router.navigateByUrl("/trainer")
  }
}
