import { Component, OnInit } from '@angular/core';
import { PokemonService } from 'src/app/services/pokemon.service';

@Component({
  selector: 'app-trainer-page',
  templateUrl: './trainer-page.html',
  styleUrls: ['./trainer-page.css']
})
export class TrainerPage implements OnInit {


  constructor(private readonly pokemonService: PokemonService) {
     pokemonService.ngOnInit()
  }

  ngOnInit(): void {
  }

}
