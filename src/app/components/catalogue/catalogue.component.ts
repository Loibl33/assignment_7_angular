import { Component, OnInit } from '@angular/core';
import {PokemonService} from "../../services/pokemon.service";
import {pokemon} from "../../models/pokemon.model";
import { StorageUtil } from 'src/app/utils/storage.utils';
import { Trainer } from 'src/app/models/trainer.model';
import { StorageKeys } from 'src/app/enums/storage-keys.enum';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.css']
})

export class CatalogueComponent implements OnInit {
  // Fields
  private _trainer?: Trainer;
  private _added: String[]
  _trainerPokemons: pokemon[] | undefined;
  pokemons : pokemon[] | undefined;
  btnClasses: any = "hidden";

  constructor( private readonly pokemonService: PokemonService) {
    this._added = []
    this._trainer = StorageUtil.storageRead<Trainer>(StorageKeys.Trainer);
    this._trainerPokemons = this._trainer?.pokemon
  }

  async ngOnInit(): Promise<void> {
    await this.pokemonService.ngOnInit()
    this.pokemons = this.pokemonService.pokemonsArrayRaw
  }

  async add(pokename : String) {
    this._added.push(pokename)
    let id = this._trainer?.id
    await this.pokemonService.addPokemon(id, pokename)
  }

  hidden(pokename: String) {
      return !this._added.includes(pokename)
  }
}
