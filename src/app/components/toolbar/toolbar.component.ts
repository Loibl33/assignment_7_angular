import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StorageKeys } from 'src/app/enums/storage-keys.enum';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})

export class ToolbarComponent implements OnInit {
  trainerName : String | undefined;

  constructor(
    private readonly router: Router,
    private readonly trainerService: TrainerService) { }

  ngOnInit(): void {

    this.trainerName = this.trainerService.trainer?.username
  }

  goToTrainerButton() {
    this.router.navigateByUrl("/trainer")
  }

  goToCatalogueButton() {
    this.router.navigateByUrl("/catalogue")
  }

  logoutButton() {
    sessionStorage.removeItem(StorageKeys.Trainer);
    this.router.navigateByUrl("landing");
  }
}
