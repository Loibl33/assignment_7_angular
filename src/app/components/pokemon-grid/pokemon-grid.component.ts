import {Component, OnInit} from '@angular/core';
import {pokemon} from "../../models/pokemon.model";
import {StorageUtil} from "../../utils/storage.utils";
import {Trainer} from "../../models/trainer.model";
import {StorageKeys} from "../../enums/storage-keys.enum";
import {PokemonService} from "../../services/pokemon.service";
import {fetchPokemonByName} from "../../services/pokemon.service";

@Component({
  selector: 'app-pokemon-grid',
  templateUrl: './pokemon-grid.component.html',
  styleUrls: ['./pokemon-grid.component.css']
})
export class PokemonGridComponent implements OnInit {
  // Fields
  pokeNames: pokemon[] | undefined
  pokemons: pokemon[] | undefined
  private _trainer?: Trainer;

  constructor(private readonly pokemonService: PokemonService) {
    // Use the services
    this._trainer = StorageUtil.storageRead<Trainer>(StorageKeys.Trainer);
    this.pokemons = []
    this.pokeNames = []
    this.pokeNames = StorageUtil.storageRead<Trainer>(StorageKeys.Trainer)?.pokemon


  }

  async ngOnInit(): Promise<void> {
    this.pokeNames = StorageUtil.storageRead<Trainer>(StorageKeys.Trainer)?.pokemon
    this.pokemons = await this.fetchCollected(this.pokeNames)
  }

  //-- Fetch a collected
   async fetchCollected(pokenames: pokemon[] | undefined) : Promise<pokemon[]>{
     let arr = []
     // @ts-ignore
     for (let p of pokenames) {
       let poke: pokemon = await fetchPokemonByName(p.toString())
       arr.push(poke)
     }
     return arr
   }

   //- Remove a pokemon
  remove(name: String) {
    this.pokemons = this.pokemons?.filter(r => r.name != name)
    // @ts-ignore
    this._trainer?.pokemon = this.pokemons
    StorageUtil.storageSave<Trainer>(StorageKeys.Trainer, this._trainer!)
    this.pokemonService.removePokemon(this._trainer?.id, name)


  }
}
