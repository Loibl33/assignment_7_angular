# Assignment 4 - Assignment_7_Angular
Input URL below
[![web](https://img.shields.io/static/v1?logo=heroku&message=Online&label=Heroku&color=430098)](https://bosman-loibl-angular-assignmen.herokuapp.com/trainer)

## Assignment
This application is a pokemon trainer web app using the Angular Framework. This project is part of the Noroff Frontend Traineeship. 

## How to use
The user can login in the Landing page and will then directly be redirected to the Trainer Page. On this page the collected pokemons of the user are displayed and the user can remove pokemons from their collection. Via a menu, the user has the option to go to the Catalogue Page, where all the pokemons are displayed. Pokemons can be added by clicking on the button.

## Use the app click on the link bellow
https://bosman-loibl-angular-assignmen.herokuapp.com/trainer

## Contributing
I am open to receive advice and tips which can help me to improve the design, the logic, to clean up the code, etc. So that I can improve myself as a developer.

## Built With
[Microsoft VSCode](https://code.visualstudio.com/)

## Technologies
- Angular Framework
- JavaScript
- HTML
- CSS
- Angular Material

## Author
[Dianto Bosman](https://gitlab.com/diantobosman1)

[Phillip Loibl](https://gitlab.com/Loibl33)

## Acknowledgment
[Dean von Schoultz](https://gitlab.com/deanvons) who is the tutor of the Frontend course at Noroff.

## Project status
Project exclusively made for study purposes.

## License
[MIT](https://choosealicense.com/licenses/mit/)
